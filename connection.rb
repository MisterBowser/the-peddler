require 'mongo'

class Connection
  def initialize(database='dev')
    @client = Mongo::Client.new(ENV['MONGODB_URI'], :database => database)
  end

  def client
    @client
  end

  def merchants
    @merchants ||= Merchants.new(@client[:merchants])
  end
end

class Merchants
  extend Forwardable

  def initialize(doc)
    @doc = doc
  end

  def find(id)
    @doc.find({ merchant_id: id })
  end

  def find_by_channel_id(id)
    @doc.find({ channel_id: id })
  end

  def filter(filter)
    @doc.find(filter)
  end

  def all
    @doc.find
  end

  def_delegators :@doc, :insert_one, :update_one, :delete_many
end