require 'discordrb'

class DieTable
  def initialize(&block)
    @possibilites = {}
    self.instance_eval(&block)
  end

  def on_a(range, *messages, &block)
    @possibilites[Array(range)] = block_given? ? lambda { instance_eval(&block) } : messages.join("\r\n")
  end

  def result_for(roll)
    @possibilites.each do |range, result|
      return result.respond_to?(:call) ? result.call() : result if range.include? roll
    end
  end
end

module CriticalCommands
  extend Discordrb::Commands::CommandContainer

  command :fumble, min_args: 1, max_args: 1, description: "Returns a random fumble effect.", usage: "!fumble [melee/ranged/thrown/natural]" do |event, type|
    fumble(type) || "Unknown Type"
  end

  def self.fumble_melee
    melee_fumble_table.result_for DieCommands.roll_die 20
  end

  def self.melee_fumble_table
    DieTable.new do
      on_a 1..2, "Weapon Break.", "The force of your blow, or parrying that of your opponent's, causes your weapon to break in two."
      on_a 3..4, "Goodbye Fair Blade!", "Roll an Strength / Athletics check DC 15, or your weapon flies d12 feet out of your hand in a random direction. If you have any movement and a bonus action left you can go and pick it up. In doing so you provoke an opportunity attack from anyone in the area, starting with your most immediate opponent. (Otherwise you could simply draw a second weapon, if you have one, using a bonus action)."
      on_a 5..6, "Wild Swing.", "You overextend yourself going for the kill. Your opponent gains advantage on their next attack roll."
      on_a 7, "Stuck Weapon.", "Your weapon gets stuck in your opponent's shield, armour, hide, or else in a tree or wall, or the ground. Roll a Strength check to see if you can free it using a bonus action. The DC is 8 + your strength modifier."
      on_a 8, "Oooops!", "You hit an unintended foe in combat. Randomise all combatants within 5 feet and roll a second attack roll, if you beat their armour class roll damage as if they were your intended target. (Discount sneak attack damage for Rogues)."
      on_a 9, "Self Inflicted Wound.", "You manage to slice yourself with your own blade, roll normal damage and half it. (Applies to combatants using slashing weapons and flails only. Other weapon types roll again. Discount sneak attack damage for Rogues)."
      on_a 10..14, "Slip Up.", "You lose your footing. Roll Dexterity / Acrobatics check (DC15) or fall prone. Your turn has ended and melee attacks have advantage on you (see p292 of the PHB for conditions of being prone)."
      on_a 15, "Pulled Muscle (Arms).", "Roll a Constitution Saving Throw DC15 or the strain of your attack causes you to pull a muscle in your upper body. You have disadvantage in attack rolls and ability checks requiring upper body strength until you have completed three long rests, or received magical healing."
      on_a 16, "Pulled Muscle (Legs).", "Roll a Constitution Saving Throw DC15 or the strain of combat causes you to pull a muscle in your leg. Your movement is halved, and you lose your dex modifier to AC and initiative, and you have disadvantage on any ability checks that require lower body strength, until you have completed three long rests, or received magical healing."
      on_a 17..18, "Loss of Nerve.", "Man your opponent looks tough. Make a Wisdom Saving Throw with a base DC of 10 modified by +2 for every hit dice higher than your opponent has (or -2 for every hit dice less). On a fail you are frightened (see p292 of the PHB). On each turn you may attempt the the saving throw."
      on_a 19, "Broken Item.", "In the hurly burly of combat, something fragile - like a magic potion - you're carrying breaks. Randomise fragile objects you have in your possession and roll to determine which."
      on_a 20, "A Little Accident.", "Either through fear, excitement, or simply needing to go, you soil yourself. 75% chance it's only pee."
    end
  end

  def self.fumble_ranged
    ranged_fumble_table.result_for DieCommands.roll_die 20
  end

  def self.ranged_fumble_table
    DieTable.new do
      on_a 1..2, "Weapon Break.", "Your bow shaft or a mechanism in your crossbow breaks and is now useless."
      on_a 3..5, "String Break.", "Your bowstring snaps. It takes 15 minutes to restring it."
      on_a 6..8, "Loose String.", "Your string comes loose. You lose this attack. Starting next turn you can make a sleight of hand check DC15 to fix it. Each attempt takes one turn."
      on_a 9..16, "Ooops.", "You hit an unintended random target. Randomise all combatants within 10 feet (for a short range attack, or 30 feet for a long range attack) and roll a second attack roll, if you beat their armour class roll damage as if they were your intended target (discount sneak attack damage for Rogues)."
      on_a 17..18, "Ammo Accident.", "Your quiver spills (50% strap broken, 50% you tilt it over by accident), and the remainder of your arrows / bolts fall to the floor. If you remain still you can use a bonus action to pick up one a round and still fire using your action. Otherwise you can use an action to pick up 2d8 and put them back in your quiver."
      on_a 19, "Pulled Muscle (Armed Upper Body).", "Roll a Constitution Saving Throw DC15 or the strains of your attack causes you to pull a muscle in your upper body. You have disadvantage in attack rolls and ability checks requiring upper body strength until you have completed three long rests, or received magical healing."
      on_a 20, "Slip Up.", "You lose your footing. Roll Dexterity / Acrobatics (DC15) or fall prone. Your turn has ended and melee attacks have advantage on you (see p292 of the PHB for conditions of being prone)."
    end
  end

  def self.fumble_thrown
    thrown_fumble_table.result_for DieCommands.roll_die 10
  end

  def self.thrown_fumble_table
    DieTable.new do
      on_a 1, "Weapon Break.", "Your bow shaft or a mechanism in your crossbow breaks and is now useless."
      on_a 2, "Pulled Muscle (Arms).", "Roll a Constitution Saving Throw DC15 or the strain of your attack causes you to pull a muscle in your upper body. You have disadvantage in attack rolls and ability checks requiring upper body strength until you have completed three long rests, or received magical healing."
      on_a 3..4, "Slip Up.", "You lose your footing. Roll Dexterity / Acrobatics (DC15) or fall prone. Your turn has ended and melee attacks have advantage on you (see p292 of PH for conditions of being prone)."
      on_a 5..9, "Ooops!", "You hit an unintended random target. Randomise all combatants within 10 feet (for a short range attack, or 30 feet for a long range attack) and roll a second attack roll, if you beat their armour class roll damage as if they were your intended target (discount sneak attack damage for Rogues)."
      on_a 10, "WTF?.", "You launch a comically bad projectile attack nowhere near your intended opponent - it flies into a huge empty space (or at the DM's discretion a distant unintended target) taking your self confidence with it. Roll a wisdom saving throw DC15, or suffer disadvantage to attack rolls until you next score a hit on an opponent."
    end
  end

  def self.fumble_natural
    natural_fumble_table.result_for DieCommands.roll_die 10
  end

  def self.natural_fumble_table
    DieTable.new do
      on_a 1..2, "Ouch!", "The attacker snaps one or several teeth/claws on its target's weapon or armour, or nearby surface. Roll 1d3. They take that much damage, and going forward subtract that much damage from this attack (unless it was a tail attack)."
      on_a 3..5, "Wild Swing.", "The attacker overextends itself going for the kill. Their intended target gains advantage on their next attack roll against them."
      on_a 6..7, "Slip Up.", "The attacker loses its footing. Roll a Dexterity/Acrobatics check (DC15) or fall prone. Their turn has ended and melee attacks have advantage on you (see p292 of the PHB for conditions of being prone). Creatures with more than two legs are immune to this effect."
      on_a 8..10, "Loss of Nerve.", "The attacker is scared. They must make a Wisdom Saving Throw with a base DC of 10 modified by +2 for every hit dice higher than the opponent has (or -2 for every hit dice less). On a fail they are frightened (see p292 of the PHB). On each turn they may attempt the the saving throw. Creates that inspire fear are immune to this effect (unless their target also inspires fear)."
    end
  end

  def self.fumble(type)
    {
      :melee => fumble_melee,
      :ranged => fumble_ranged,
      :thrown => fumble_thrown,
      :natural => fumble_natural
    }.fetch(type.downcase.intern, nil)
  end

  command :crit, min_args: 1, max_args: 1, description: "Returns a critical success effect.", usage: "!crit [slashing/bludgeoning/piercing/fire/cold/lightning/force/necrotic/radiant/acid/psychic/thunder]" do |event, type|
    crit(type) || "Unknown Type"
  end

  def self.crit(type)
    {
      :slashing => crit_slashing,
      :bludgeoning => crit_bludgeoning,
      :piercing => crit_piercing,
      :fire => crit_fire,
      :cold => crit_cold,
      :lightning => crit_lightning,
      :force => crit_force,
      :necrotic => crit_necrotic,
      :radiant => crit_radiant,
      :acid => crit_acid,
      :psychic => crit_psychic,
      :thunder => crit_thunder
    }.fetch(type.downcase.intern, nil)
  end

  def self.crit_slashing
    slashing_crit_table.result_for DieCommands.roll_die 100
  end

  def self.slashing_crit_table
    DieTable.new do
      on_a 1..3, "Gruesome Slash", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Debilitating Cut", "Roll one extra die of the weapon's damage to the target."
      on_a 7..9, "Vicious Laceration", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 damage."
      
      on_a 10, "Horrific Gash", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal Wound", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Nasty Slice", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Savage Chop", "The target is also knocked prone."
      
      on_a 20, "Inspiring Stroke", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Ruthless Assault", "As a free action you may immediately make one melee attack vs. the same target."
      on_a 24..26, "Nicked an Artery", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 damage every rd. until it saves."
      on_a 27..29, "Bloody Trauma", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
     
      on_a 30, "Cleaving Hack", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling Attack", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Nauseating Injury", "The target is stunned for 1 rd."
      on_a 37..39, "Flesh-rending Strike", "The target is now vulnerable to slashing damage for the remainder of the encounter."
      
      on_a 40, "Monstrous Damage", "The target suffers triple damage."
      on_a 41..43, "Torturous Impairment", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Shocking Violence", "You receive advantage for all melee attacks vs. this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing Pain", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Severing Strike", "The target's off-hand is cut off. The target has disadvantage for the remainder of the encounter and 1d10 damage every rd. until healed."
      on_a 51..53, "Hellish Distress", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_slashing(), CriticalCommands.crit_slashing()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked Mutilation", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Calamitous Blow", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous Punishment", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile Suffering", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack."
      on_a 67..69, "Ruinous Harm", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 damage."
      
      on_a 70, "Slow and Agonizing Death", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 damage every rd. until it saves."
      on_a 71..73, "Dire Consequences", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating Damage", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing Anguish", "You receive advantage for all melee attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Maimed", "The target's arm is severed. It suffers disadvantage for the remainder of the encounter and suffers 2d10 damage every rd. until healed."
      on_a 81..83, "Gutted", "The target suffers triple damage and is incapacitated for 1 rd"
      on_a 84..86, "Gaping Wound", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing Disfigurement", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."
      
      on_a 90, "Severed Limb", "The target's arm is severed. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Rent Armor", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "Disemboweled", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating Cost", "As a free action you may immediately make one melee attack with advantage vs. the same target."
      
      on_a 100, "Decapitated", "The target is slain."
    end
  end

  def self.crit_bludgeoning
    bludgeoning_crit_table.result_for DieCommands.roll_die 100
  end

  def self.bludgeoning_crit_table
    DieTable.new do
      on_a 1..3, "Crushing blow.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Debilitating smash.", "Roll one extra die of the weapon's damage to the target."
      on_a 7..9, "Shattering wallop.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 damage."

      on_a 10, "Bone-crushing force.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating strike.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Savage pounding.", "The target is also knocked prone."
      
      on_a 20, "Inspiring stroke.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Ruthless assault.", "As a free action you may immediately make one melee attack vs. the same target."
      on_a 24..26, "Smashed.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 damage every rd. until it saves."
      on_a 27..29, "Bruising trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Pulverizing crunch.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Nauseating injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Pummeled.", "The target is now vulnerable to bludgeoning damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Shocking violence.", " You receive advantage for all melee attacks vs. this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Severing strike.", "The target's off-hand is cut off. The target has disadvantage for the remainder of the encounter and 1d10 damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_bludgeoning(), CriticalCommands.crit_bludgeoning()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Calamitous blow.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack."
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all melee attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Broken bones.", "The target's arm is broken. It suffers disadvantage for the remainder of the encounter and suffers 2d10 damage every rd. until healed."
      on_a 81..83, "Fracturing bash.", " The target suffers triple damage and is incapacitated for 1 rd."
      on_a 84..86, "Crushed.", " The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."
      
      on_a 90, "Ruined limb.", "The target's arm is crushed. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Crumpled armor.", "The target's AC is reduced by 2 for the remainder of the encounter.         "
      on_a 94..96, "Massive thrashing.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed.         "
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one melee attack with advantage vs. the same target."
      on_a 100, "Blown apart.", "The target is slain."
    end
  end

  def self.crit_piercing
  end

  def self.piercing_crit_table
    DieTable.new do
      on_a 1..3, "Piercing lunge.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Debilitating stab.", "Roll one extra die of the weapon's damage to the target."
      on_a 7..9, "Shattering thrust.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 damage."
      
      on_a 10, "Perforated.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating strike.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Ventilated.", "The target is also knocked prone."
      
      on_a 20, "Inspiring stroke.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Ruthless assault.", "As a free action you may immediately make one melee attack vs. the same target."
      on_a 24..26, "Skewered.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 damage every rd. until it saves."
      on_a 27..29, "Ghastly trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Run through.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Nauseating injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Punctured.", "The target is now vulnerable to piercing damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Shocking violence.You receive advantage for all melee attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Penetrating strike.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_piercing(), CriticalCommands.crit_piercing()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Stinging blow.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all melee attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, 80, "Speared.", "The target's arm is rendered useless. It suffers disadvantage for the remainder of the encounter and suffers 2d10 damage every rd. until healed."
      on_a 81..83, "Impaled.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Pierced artery.The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."

      on_a 90, "Blinded.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Punctured armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "Bullseye.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one melee attack with advantage vs. the same target."
      on_a 100, "Killing blow.", "The target is slain."
    end
  end

  def self.crit_fire
    fire_crit_table.result_for DieCommands.roll_die 100
  end

  def self.fire_crit_table
    DieTable.new do
      on_a 1..3, "Burning pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Flickering flames.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Withering heat.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 fire damage."
      
      on_a 10, "Smoldering.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating inferno.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Smoking ruin.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Blistering assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Roasted.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 fire damage every rd. until it saves."
      on_a 27..29, "Ghastly trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Fuming conflagration.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Smoldering injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Seared.", "The target is now vulnerable to fire damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Scalding violence.You receive advantage for all attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Fiery doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 fire damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_fire(), CriticalCommands.crit_fire()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Catastrophic burning.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 fire damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 fire damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Consumed.", "The target's arm is burnt to ash. It suffers disadvantage for the remainder of the encounter and 2d10 fire damage every rd. until healed."
      on_a 81..83, "Scorching devastation.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Burnt to ash.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."

      on_a 90, "Cauterized.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Melted armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "All-consuming flame.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Vaporized.", "The target is slain."
    end
  end

  def self.crit_cold
  end

  def self.cold_crit_table
    DieTable.new do
      on_a 1..3, "Freezing pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Iced over.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Shivering cold.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 cold damage."
      
      on_a 10, "Numbing effect.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Crystallize.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Winter's wrath.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Steaming ruin.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Frigid assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Roasted.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 cold damage every rd. until it saves."
      on_a 27..29, "Ghastly trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Chilling spasm.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-congealing attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Tundra's fist.", "The target is stunned for 1 rd."
      on_a 37..39, "Chilled.", "The target is now vulnerable to cold damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Snowblind.You receive advantage for all attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Killing frost.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 cold damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_cold(), CriticalCommands.crit_cold()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Deep freeze.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 cold damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 cold damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Icy grip.", "The target's arm shatters. It suffers disadvantage for the remainder of the encounter and 2d10 cold damage every rd. until healed."
      on_a 81..83, "Wintry devastation.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Snowstruck.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."

      on_a 90, "Frostbitten.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Shattered armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "Cold as the grave.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Frozen solid.", "The target is slain."
    end
  end

  def self.crit_lightning

  end

  def self.lightning_crit_table
    DieTable.new do
      on_a 1..3, "Shocking pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Crackling flashes.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Lingering sparks.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 lightning damage."
      
      on_a 10, "Electrified.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating bolt.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Sparking ruin.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Jarring assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Zapped.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 lightning damage every rd. until it saves."
      on_a 27..29, "Frazzling trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Electrifying agony.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Convulsing injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Jolted.", "The target is now vulnerable to lightning damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Stunning violence.You receive advantage for all attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Twitching doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 lightning damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_lightning(), CriticalCommands.crit_lightning()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Shocked.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 lightning damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 lightning damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Consumed.", "The target's arm is burnt to ash. It suffers disadvantage for the remainder of the encounter and 2d10 fire damage every rd. until healed."
      on_a 81..83, "Staggering charge.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Lingering static.The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."

      on_a 90, "Cauterized.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Fused armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "Sizzled.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Electrocuted.", "The target is slain."
    end
  end

  def self.crit_force
    force_crit_table.result_for DieCommands.roll_die 100
  end

  def self.force_crit_table
    DieTable.new do
      on_a 1..3, "Supernatural shock.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Flickering chaos.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Withering arcana.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 force damage."
      
      on_a 10, "Seething wreck.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal magicks.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating wizardry.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Blasted ruin.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Preternatural assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Ensorcelled.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 force damage every rd. until it saves."
      on_a 27..29, "Ghastly trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Arcane conflagration.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Dweomer-struck injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Wracked.", "The target is now vulnerable to force damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Supranatural violence.You receive advantage for all attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Eldritch doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 force damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_force(), CriticalCommands.crit_force()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Catastrophic effect.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 force damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 force damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Consumed.", "The target's arm is burnt to ash. It suffers disadvantage for the remainder of the encounter and 2d10 force damage every rd. until healed."
      on_a 81..83, "Sorcerous devastation.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Burnt to ash.The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."
  
      on_a 90, "Cauterized.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Chaos-touched armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "All-consuming power.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Disintegrated.", "The target is slain."
    end
  end

  def self.crit_necrotic
    necrotic_crit_table.result_for DieCommands.roll_die 100
  end

  def self.necrotic_crit_table
    DieTable.new do
      on_a 1..3, "Rotting pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Rattle of bones.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Withering necromancy.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 necrotic damage."
      
      on_a 10, "Condemned.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastation.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Morbid ruin.", "The target is also knocked prone."
      
      on_a 20, "Intimidating blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Grim assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Final judgment.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 necrotic damage every rd. until it saves."
      on_a 27..29, "Ghastly trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Doomed.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Grave injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Calamitous loss .", "The target is now vulnerable to necrotic damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Necromantic violence.You receive advantage for all attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Necrotic doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 necrotic damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_necrotic(), CriticalCommands.crit_necrotic()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Terrifying effect.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 necrotic damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 necrotic damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Withered.", "The target's arm is rotted away. It suffers disadvantage for the remainder of the encounter and 2d10 necrotic damage every rd. until healed."
      on_a 81..83, "Dark finality.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Lost cause.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."
      
      on_a 90, "Abyssal decay.", "The target is made blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Decayed armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "Hungry death.", "The target has disadvantage for the rest of the encounter and the damage rolled for the attack each round until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."

      on_a 100, "Termination.", "The target is slain."
    end
  end

  def self.crit_radiant
    radiant_crit_table.result_for DieCommands.roll_die 100
  end

  def self.radiant_crit_table
    DieTable.new do
      on_a 1..3, "Glorious pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Purifying flames.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Virtuous glory.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 radiant damage."
      
      on_a 10, "Cleansed.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating righteousness.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Luminous ruin.", "The target is also knocked prone."
      
      on_a 20, "Inspiring aura.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Exalted assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Scintillating glow.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 radiant damage every rd. until it saves."
      on_a 27..29, "Cleansing trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Shining splendor.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Smoldering injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Dazzled.", "The target is now vulnerable to radiant damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Magisterial violence.You receive advantage for all attacks vs.", "this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Heaven's wrath.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 radiant damage every rd. until healed."
      on_a 51..53, "Angelic vengeance.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_radiant(), CriticalCommands.crit_radiant()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Divine wrath.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Celestial punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 radiant damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 radiant damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Hallowed.", "The target's arm is burnt to ash. It suffers disadvantage for the remainder of the encounter and 2d10 radiant damage every rd. until healed."
      on_a 81..83, "Scorching devastation.The target suffers triple damage and is incapacitated for 1 rd.", ""
      on_a 84..86, "Everlasting shame.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."

      on_a 90, "Judged.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Shriven armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "All-consuming radiance.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Purified.", "The target is slain."
    end
  end

  def self.crit_acid
    acid_crit_table.result_for DieCommands.roll_die 100
  end

  def self.acid_crit_table
    DieTable.new do
      on_a 1..3, "Burning acid.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Choking fumes.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Acrid bile.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 acid damage."
      
      on_a 10, "Bitter pungency.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Bubbling grief.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Deteriorating ruin.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Decomposing assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Scoured.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 acid damage every rd. until it saves."
      on_a 27..29, "Corroding trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Fuming disruption.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Biting injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Corroded.", "The target is now vulnerable to acid damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Liquefying violence.", "You receive advantage for all attacks vs. this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Caustic doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 acid damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_acid(), CriticalCommands.crit_acid()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Catastrophic misery.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 acid damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 acid damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Consumed.", "The target's arm is dissolved. It suffers disadvantage for the remainder of the encounter and 2d10 acid damage every rd. until healed."
      on_a 81..83, "Acidic devastation.", "The target suffers triple damage and is incapacitated for 1 rd."
      on_a 84..86, "Eaten away.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."
      
      on_a 90, "Cauterized.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Dissolving armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "All-consuming flame.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Dissolved.", "The target is slain."
    end
  end

  def self.crit_psychic
    psychic_crit_table.result_for DieCommands.roll_die 100
  end

  def self.psychic_crit_table
    DieTable.new do
      on_a 1..3, "Psychic pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Nightmarish visions Roll one extra die of the attack's damage to the target.", ""
      on_a 7..9, "Broken will.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 psychic damage."
      
      on_a 10, "Mind-blasted.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal effect.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating psychosis.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Wasted.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Mental assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Unhinged.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 psychic damage every rd. until it saves."
      on_a 27..29, "Subliminal trauma.", "The target's melee attacks only deal half damage for the remainder of the encounter unless it makes a DC 10 CON Save."
      
      on_a 30, "Paranoid delusions.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Lingering injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Touched.", "The target is now vulnerable to psychic damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Psionic violence.", "You receive advantage for all attacks vs. this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing pain.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Psychotic doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 psychic damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_psychic(), CriticalCommands.crit_psychic()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Unnerved.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Catastrophic raving.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 psychic damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 psychic damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Catatonic.", "The target suffers disadvantage for the remainder of the encounter and 2d10 psychic damage every rd. until healed."
      on_a 81..83, "Rational devastation.", "The target suffers triple damage and is incapacitated for 1 rd."
      on_a 84..86, "Cognitive disruption.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing ordeal.", "The target suffers a permanent -2 loss to its CHA due to stress-related injuries."
      
      on_a 90, "Dumbstruck.", "The target is rendered blind. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Rendered defenseless.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "Shattered mind.", "The target has disadvantage for the rest of the encounter and suffers the damage rolled each rd. until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Lobotomized.", "The target is slain."
    end
  end

  def self.crit_thunder
    thunder_crit_table.result_for DieCommands.roll_die 100
  end

  def self.thunder_crit_table
    DieTable.new do
      on_a 1..3, "Deafening pain.", "The target must make a successful DC 10 CON Save or receive disadvantage for its next attack."
      on_a 4..6, "Thundering roar.", "Roll one extra die of the attack's damage to the target."
      on_a 7..9, "Earsplitting crash.", "The target must make a successful DC 10 CON Save or suffer an additional 1d8 thunder damage."
      
      on_a 10, "Reverberating agony.", "The target loses its next attack as it staggers in shock from its wound."
      on_a 11..13, "Brutal wound.", "The target must make a successful DC 10 CON Save or its speed is halved for the remainder of the encounter."
      on_a 14..16, "Devastating barrage.", "Reroll all 1s and 2s on the damage roll for this attack."
      on_a 17..19, "Wave of sound.", "The target is also knocked prone."
      
      on_a 20, "Inspiring blast.", "Your allies within 30 feet gain a d6 inspiration die that can be used during this encounter."
      on_a 21..23, "Booming assault.", "As a free action you may immediately make one attack vs. the same target."
      on_a 24..26, "Detonated.", "The target must make a successful DC 12 CON Save or suffer and additional 1d8 thunder damage every rd. until it saves."
      on_a 27..29, "Ghastly trauma.", "The target's melee attacks only deals half damage for the remainder of the encounter unless it succeeds on a DC 10 CON Save."
      
      on_a 30, "Cacaphony.", "One adjacent ally of the target is also struck by this attack and suffers the equivalent of half the inflicted damage."
      on_a 31..33, "Blood-curdling attack.", "The target becomes frightened for the remainder of the encounter."
      on_a 34..36, "Thunderous injury.", "The target is stunned for 1 rd."
      on_a 37..39, "Inescapable fury.", "The target is now vulnerable to thunder damage for the remainder of the encounter."
      
      on_a 40, "Monstrous damage.", "The target suffers triple damage."
      on_a 41..43, "Torturous impairment.", "The target becomes incapacitated for 1 rd."
      on_a 44..46, "Explosive violence.", "You receive advantage for all attacks vs. this opponent for the remainder of the encounter."
      on_a 47..49, "Traumatizing effect.", "The target becomes exhausted to level 4 of that condition."
      
      on_a 50, "Noisy doom.", "The target has disadvantage for the remainder of the encounter and suffers 1d10 thunder damage every rd. until healed."
      on_a 51..53, "Hellish distress.", "The target suffers the effects of a bane spell for the remainder of the encounter."
      on_a 54..56 do
        [CriticalCommands.crit_thunder(), CriticalCommands.crit_thunder()].join('\r\ncombined with\r\n')
      end
      on_a 57..59, "Wicked mutilation.", "The target suffers a permanent -1 loss to its CHA due to horrible scarring."
      
      on_a 60, "Sonic wave.", "The target must make a successful DC 10 DEX save or it drops whatever it has in hand."
      on_a 61..63, "Heinous punishment.", "The target's allies all suffer disadvantage for their next attack."
      on_a 64..66, "Vile suffering.", "The target must make a successful DC 15 CON Save or receive disadvantage for its next attack"
      on_a 67..69, "Ruinous harm.", "The target must make a successful DC 14 CON Save or suffer an additional 1d12 thunder damage."
      
      on_a 70, "Slow and agonizing death.", "The target must make a successful DC 15 CON Save or suffer an additional 2d8 thunder damage every rd. until it saves."
      on_a 71..73, "Dire consequences.", "Your allies receive advantage on all attacks vs. the target until the start of your next turn."
      on_a 74..76, "Excruciating damage.", "Reroll all 1s and 2s and 3s on the damage roll for this attack."
      on_a 77..79, "Vexing anguish.", "You receive advantage for all attacks vs. the target and the target has disadvantage for the remainder of the encounter."
      
      on_a 80, "Blown to pieces.", "The target suffers disadvantage for the remainder of the encounter and 2d10 thunder damage every rd. until healed."
      on_a 81..83, "Rumbling devastation.", "The target suffers triple damage and is incapacitated for 1 rd."
      on_a 84..86, "Thunderstruck.", "The target suffers the damage rolled for the attack each round until healed."
      on_a 87..89, "Harrowing disfigurement.", "The target suffers a permanent -2 loss to its CHA due to horrible scarring."
      
      on_a 90, "Cannonade.", "The target is rendered deaf. It suffers disadvantage for the remainder of the encounter and suffers a 50% HP loss every rd. until healed."
      on_a 91..93, "Broken armor.", "The target's AC is reduced by 2 for the remainder of the encounter."
      on_a 94..96, "All-consuming clamor.", "The target has disadvantage for the rest of the encounter and the damage rolled for the attack each round until healed."
      on_a 97..99, "Devastating cost.", "As a free action you may immediately make one attack with advantage vs. the same target."
      on_a 100, "Imploded.", "The target is slain."
    end
  end
end