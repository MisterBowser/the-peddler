require 'discordrb'

module CurrencyUtilities
  extend Discordrb::Commands::CommandContainer

  @currencies = [
    "PP",
    "GP",
    "EP",
    "SP",
    "CP"
  ]

  command(:pp, min_args: 1, max_args: 1, description: "Converts the given PP to other currencies.") do |event, pp|
    convert_from pp, "PP"
  end
  
  command(:gp, min_args: 1, max_args: 1, description: "Converts the given GP to other currencies.") do |event, gp|
    convert_from gp, "GP"
  end
  
  command(:ep, min_args: 1, max_args: 1, description: "Converts the given EP to other currencies.") do |event, ep|
    convert_from ep, "EP"
  end
  
  command(:sp, min_args: 1, max_args: 1, description: "Converts the given SP to other currencies.") do |event, sp|
    convert_from sp, "SP"
  end
  
  command(:cp, min_args: 1, max_args: 1, description: "Converts the given CP to other currencies.") do |event, cp|
    convert_from cp, "CP"
  end

  def self.convert_from(amount, from)
    amount = amount.to_i
    ret = "#{format_number(amount)} #{from} is:\r\n"
    (@currencies - [from]).each do |cur|
      ret << "#{convert(amount, from, cur)} #{cur}\r\n"
    end
    
    ret
  end

  def self.convert(amount, from, to)
    {
      "PP": lambda { |x| x / 1000 },
      "GP": lambda { |x| x / 100 },
      "EP": lambda { |x| x / 50 },
      "SP": lambda { |x| x / 10 },
      "CP": lambda { |x| x }
    }[to.upcase.intern].call(convert_to_cp(amount, from))
  end

  def self.convert_to_cp(amount, fmt)
    {
      "PP": lambda { |x| x * 1000 },
      "GP": lambda { |x| x * 100 },
      "EP": lambda { |x| x * 50 },
      "SP": lambda { |x| x * 10 },
      "CP": lambda { |x| x }
    }[fmt.upcase.intern].call(amount)
  end

  def self.format_number(number)
    "%g" % ("%.2f" % number)
  end
end