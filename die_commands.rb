require 'discordrb'
require 'dentaku'

module DieCommands
  @@die_roll_regex = /(\d*)d(\d+)/
  extend Discordrb::Commands::CommandContainer

  command(:attempt_to, min_args: 1, description: "Success/fail generator.", usage: '!attempt_to do a Super awesome thing') do |event, *args|
    attempt = args.join(' ')
    msg = event.respond("Attempting to '#{attempt}'...")
    sleep 1
    msg.edit "Attempting to '#{attempt}'...#{rand.round == 0 ? 'Success!' : 'Failure.'}"
  end
  
  [4, 6, 8, 12, 20, 100].each do |size|
    command "d#{size}".intern, description: "Rolls a 1d#{size}" do |event|
      roll_die size
    end
  end
  
  command :roll, description: "Evaluates an equation. Complete with dice rolling!", usage: '!roll 1d4 + 2' do |event, *args|
    exp = parse_roll(args.join(' '))
  
    Dentaku::Calculator.new.evaluate(exp).to_s + " [#{exp}]"
  end

  command :roll_drop, description: "Similar to !roll, but drops the X lowest", usage: '!roll_drop 1d4 + 2' do |event, *args|
    drop = args.shift.to_i
    exp = parse_roll_drop(args.join(' '), drop)
  
    Dentaku::Calculator.new.evaluate(exp).to_s + " [#{exp}]"
  end

  def self.parse_roll(roll)
    roll.gsub(@@die_roll_regex) do
      _, count, size = Regexp.last_match.to_a
      rolls = roll_dice(count, size)

      rolls.length > 1 ? '(' + rolls.join(' + ') + ')' : rolls.first.to_s
    end
  end

  def self.parse_roll_drop(roll, drop)
    roll.gsub(@@die_roll_regex) do
      _, count, size = Regexp.last_match.to_a
      rolls = roll_dice(count, size).sort.reverse.take(count.to_i - drop)

      rolls.length > 1 ? '(' + rolls.join(' + ') + ')' : rolls.first.to_s
    end
  end

  def self.roll_dice(count, size)
    (0...[count.to_i, 1].max).collect { |i| roll_die size.to_i }
  end
  
  def self.roll_die(max)
    1 + rand(max)
  end
end