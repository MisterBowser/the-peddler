class MagicItemsList
  extend Forwardable

  def_delegators :@items, :<<, :length, :all?, :include?, :+, :map, :sample

  def initialize(items=[])
    @items = items
  end

  def to_a
    [*@items]
  end

  def +(other)
    MagicItemsList.new @items + other.to_a
  end

  def pluck(*args)
    @items.map { |item| item.delete_if {|k,v| !args.include?(k) } }
  end

  def sort(by = nil)
    if by
      MagicItemsList.new @items.sort(by)
    else
      MagicItemsList.new(@items.sort { |x, y| MagicItems.rarities.index(y[:rarity]) <=> MagicItems.rarities.index(x[:rarity]) })
    end
  end
end