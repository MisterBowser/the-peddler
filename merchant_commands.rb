require 'discordrb'
require_relative 'connection'
require_relative 'currency_utilities'

module MerchantCommands
  extend Discordrb::Commands::CommandContainer

  def self.init(conn)
    @merchants = conn.merchants
  end
  
  command(:merchant, description: "Shows the information for this channel's merchant") { |event| display_merchant_info event.channel.id }

  def self.display_merchant_info(channel_id)
    merchant = @merchants.find_by_channel_id(channel_id).first
    return "No merchant found in this channel" unless merchant

    "#{merchant[:name]}\r\n#{merchant[:description]}"
  end

  command(:add_merchant, min_args: 1, description: "Add a merchant to this channel.", usage: "!add_merchant 'merchant name' 'merchant description'", permission_level: 2) do |event, name, description|
    add_merchant(event.channel.id, name, description) ? "Added merchant!" : "Error adding merchant :(."
  end

  def self.add_merchant(channel_id, name, description)
    name = clean_argument(name)
    merchant_id = generate_unique_id(name.downcase.split.map(&:chr).join)
  
    @merchants.insert_one({
      merchant_id: merchant_id,
      name: clean_argument(name),
      description: clean_argument(description || ''),
      items: [],
      channel_id: channel_id
    }).n > 0
  end
  
  def self.clean_argument(arg)
    arg.gsub(/^'+|'+$/, '').strip
  end
  
  def self.generate_unique_id(name)
    return name unless @merchants.find(name).any?
    generate_unique_id name.succ
  end
  
  command(:add_items, min_args: 4, description: "Add items to this channel's merchant.", usage: "!add_items ([Name] [Price In GP] [Description] [Quantity] [Rarity])", permission_level: 2) do |event, *args|
    merchant = @merchants.find_by_channel_id(event.channel.id).first
    return "No merchant found in this channel." unless merchant

    res = add_items(merchant, *args)
    if res.is_a? String
      res
    else
      res ? "Added items!" : "Error adding items :/"
    end
  end

  def self.add_items(merchant, *args)
    item_arg_count = 5
  
    return "Each item must have a name, price, description, quantity, rarity." unless args.length % item_arg_count == 0
  
    args.each_slice(item_arg_count).map do |item|
      add_item(merchant, name: item[0], price: item[1], description: item[2], quantity: item[3], rarity: item[4])
    end.all?
  end

  def self.add_item(merchant, item)
    item_id = generate_unique_item_id merchant, (merchant[:items].length + 1).to_s

    @merchants.update_one({ channel_id: merchant[:channel_id] }, {
      '$push' => {
        items: {
          item_id: item_id,
          name: item[:name],
          price: item[:price],
          description: item[:description],
          quantity: item[:quantity],
          rarity: item[:rarity]
        }
      }
    }, { upsert: true }).n > 0
  end

  def self.generate_unique_item_id(merchant, id)
    return id unless merchant[:items].any? { |item| item[:item_id] == id }
    generate_unique_id merchant, id.succ
  end

  command :inventory, description: "Lists the merchants inventory." do |event|
    merchant = @merchants.find_by_channel_id(event.channel.id).first
    return "No merchant found in this channel." unless merchant
    list_inventory merchant
  end

  def self.list_inventory(merchant)
    price_display = lambda do |gp|
      disp = "#{CurrencyUtilities.convert(gp, "GP", "PP")} PP/"
      disp << "#{gp} GP/"
      disp << "#{CurrencyUtilities.convert(gp, "GP", "EP")} EP/"
      disp << "#{CurrencyUtilities.convert(gp, "GP", "SP")} SP/"
      disp << "#{CurrencyUtilities.convert(gp, "GP", "CP")} CP"
    end

    items = merchant[:items].map do |item|
      [
        "'#{item[:name]}' (#{item[:rarity]}) - ##{item[:item_id]}",
        "#{price_display.call(item[:price].to_f)} GP - #{item[:quantity]} left",
        "#{item[:description]}"
      ].join "\r\n"
    end.join "\r\n\r\n"

    items.empty? ? 'No items found for that merchant.' : "#{merchant[:name]} has:\r\n" + items
  end
end