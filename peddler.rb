require 'discordrb'
require_relative 'currency_utilities'
require_relative 'merchant_commands'
require_relative 'die_commands'
require_relative 'critical_commands'
require_relative 'connection'
require_relative 'magic_items'

#TODO

conn = Connection.new

MerchantCommands.init conn

bot = Discordrb::Commands::CommandBot.new token: ENV["BOT_TOKEN"], prefix: '!', advanced_functionality: true, help_command: :peddler_help
bot.include! CurrencyUtilities
bot.include! MerchantCommands
bot.include! DieCommands
bot.include! CriticalCommands

bot.set_role_permission(377898590477221908, 2)

bot.command :loot, min_args: 2, max_args: 6, description: "Return random magic items, with optional weighting.", usage: "!loot count max_rarity [weights*]" do |event, *args|
  #common, uncommon, rare, very rare, legendary
  if args.length == 2
    #return X random items with maximum rarity of Y
    items = MagicItems.sample(args[1], args[0].to_i)
  else
    #return X random items with maximum rarity of Y with weighting
    items = MagicItems.sample(args[1], args[0].to_i, *args.drop(2))
  end

  items.sort.map do |item|
    str = "#{item[:name]} - #{item[:type]} - #{item[:rarity]} - #{item[:source]}\r\n"
    str << "#{item[:attunment] ? 'Requires attunement.' : 'No attunement required.'}\r\n"
    str << "Notes: #{item[:notes]}\r\n" unless item[:notes].empty?

    str
  end.join "\r\n"
end

bot.command :roll_joke do |event|
  [
    "How many Paladins does it take to change a lightbulb?\r\n2. One to put in the new bulb, and another to 'uphold the light'",
    "A group of four players form a party for a new campaign. Player one creates a paladin, to provide buffs and give the party a meatshield. Player two decides on a rogue, to serve as the party's dedicated trapfinder and lockpicker. Player three wanted to be a ranger, to help the party track enemies. Player four chose a wizard, to wipe out their enemies with arcane might.\r\n\r\nUpon taking heavy injuries during their first combat and having no way to heal, the party realized they had made a clerical error.",
    "How many half-elves does it take to screw in a light bulb.\r\nJust one. Turns out, they're actually good for something.",
    "A human and an elf walk into a bar.\r\nThe dwarf and halfling walk under it.",
    "How do you know if there's a Paladin in the party?\r\nTrust me, you'll know.",
    "An Ogre walks into a bar with flint and steel. The bartender lets him in but says, 'Don't start anything'.",
    "Leather armour is the best for sneaking because it's literally made of hide.",
    "What do you call a singing pirate?\r\nA Barrrd.",
    "Our dragonborn always stays up late writing.\r\nTurns out he's talking with his parents via scale mail.",
    "So a Wizard asked our Dragonborn Barbarian how well he could count on a scale from one to ten...\r\nOur Dragonborn replied that he prefers to count on his fingers.",
    "Why are rogues better at tongue twisters than wizards?\r\nWizards cantrip up, thieves cant."
  ].sample
end

bot.run