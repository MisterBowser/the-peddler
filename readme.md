# The Peddler
> DnD Discord Bot

## Prerequisites
1. Ruby
2. Bundler
3. MongoDB (For running the bot locally)

## Running the darn thing

You'll need to set the BOT_TOKEN environment variable first.

Then:
```sh
cd the-peddler
bundle install

bundle exec ruby peddler.rb
```

## Testing
Just run RSpec:
```sh
bundle exec rspec
```

Which will run all of the specs under `spec/`.