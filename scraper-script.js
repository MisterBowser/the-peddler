var table = document.getElementById('item_list');

var data = Array.from(table.rows).map(function(item) {
  var cells = Array.from(item.cells);

  return [
    `name: ${cells[0].innerHTML}`,
    `type: ${cells[1].innerHTML}`,
    `rarity: ${cells[2].innerHTML}`,
    `attunement: ${cells[3].innerHTML.length > 0}`,
    `notes: ${cells[4].innerHTML}`,
    `source: ${cells[5].innerHTML}`
  ].join(',\r\n');
});

console.log(data);