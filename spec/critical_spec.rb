require 'RSpec'
require_relative '../critical_commands'

RSpec.describe CriticalCommands do
  %i(melee ranged).each do |type|
    it "has a case for every fumble roll for #{type}" do
      table = CriticalCommands.send("#{type}_fumble_table")
      for i in 1..20 do
        expect(table.result_for(i)).to be_a(String)
      end
    end
  end

  %i(thrown natural).each do |type|
    it "has a case for every fumble roll for #{type}" do
      table = CriticalCommands.send("#{type}_fumble_table")
      for i in 1..10 do
        expect(table.result_for(i)).to be_a(String)
      end
    end
  end

  %i(slashing bludgeoning piercing fire cold lightning force necrotic radiant acid psychic thunder).each do |type|
    it "has a case for every crit roll for #{type}" do
      table = CriticalCommands.send("#{type}_crit_table")
      for i in 1..100 do
        expect(table.result_for(i)).to be_a(String)
      end
    end
  end
end