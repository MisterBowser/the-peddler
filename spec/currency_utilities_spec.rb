require 'RSpec'
require_relative '../currency_utilities'

RSpec.describe CurrencyUtilities do
  it 'converts PP to *' do
    expect(convert(103, "PP", "PP")).to eq(103)
    expect(convert(103, "PP", "GP")).to eq(103 * 10)
    expect(convert(103, "PP", "EP")).to eq(103 * 20)
    expect(convert(103, "PP", "SP")).to eq(103 * 100)
    expect(convert(103, "PP", "CP")).to eq(103 * 1000)
  end

  it 'converts GP to *' do
    expect(convert(103, "GP", "PP")).to eq(103 / 10)
    expect(convert(103, "GP", "GP")).to eq(103)
    expect(convert(103, "GP", "EP")).to eq(103 * 2)
    expect(convert(103, "GP", "SP")).to eq(103 * 10)
    expect(convert(103, "GP", "CP")).to eq(103 * 100)
  end

  it 'converts EP to *' do
    expect(convert(103, "EP", "PP")).to eq(103 / 20)
    expect(convert(103, "EP", "GP")).to eq(103 / 2)
    expect(convert(103, "EP", "EP")).to eq(103)
    expect(convert(103, "EP", "SP")).to eq(103 * 5)
    expect(convert(103, "EP", "CP")).to eq(103 * 50)
  end

  it 'converts SP to *' do
    expect(convert(103, "SP", "PP")).to eq(103 / 100)
    expect(convert(103, "SP", "GP")).to eq(103 / 10)
    expect(convert(103, "SP", "EP")).to eq(103 / 5)
    expect(convert(103, "SP", "SP")).to eq(103)
    expect(convert(103, "SP", "CP")).to eq(103 * 10)
  end

  it 'converts CP to *' do
    expect(convert(103, "CP", "PP")).to eq(103 / 1000)
    expect(convert(103, "CP", "GP")).to eq(103 / 100)
    expect(convert(103, "CP", "EP")).to eq(103 / 50)
    expect(convert(103, "CP", "SP")).to eq(103 / 10)
    expect(convert(103, "CP", "CP")).to eq(103)
  end

  def convert(amount, from, to)
    CurrencyUtilities.convert(103, from, to)
  end
end