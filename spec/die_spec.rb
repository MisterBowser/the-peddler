require 'RSpec'
require_relative '../die_commands'

RSpec.describe DieCommands do
  it 'handles simple expressions' do
    expect(DieCommands.parse_roll('1')).to eq('1')
    expect(DieCommands.parse_roll('5 + 5')).to eq('5 + 5')
  end

  it 'handles die rolls' do
    expect(DieCommands.parse_roll('1d4')).not_to eq('1d4')
    expect(DieCommands.parse_roll('1d4')).not_to eq("()")
  end
end