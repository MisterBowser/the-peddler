require 'RSpec'
require_relative '../magic_items'

RSpec.describe MagicItems do
  MagicItems.rarities.each do |rarity|
    method_name_rarity = rarity.gsub(' ', '_').downcase
    it "returns items for #{rarity} rarity" do
      expect(MagicItems.send(method_name_rarity.intern).all? { |item| item[:rarity] == rarity }).to eq(true)
    end

    it "returns items up to (and including) #{rarity} rarity" do
      rarities = MagicItems.rarities.take(MagicItems.rarities.index(rarity) + 1)
      expect(MagicItems.send(:"up_to_#{method_name_rarity}").all? { |item| rarities.include? item[:rarity] }).to eq(true)
    end
  end

  it "should sample items" do
    expect(MagicItems.sample("legendary", 5).length).to eq(5)
    expect(MagicItems.sample("legendary", 5, "uncommon").length).to eq(5)
  end
end