require 'RSpec'
require_relative '../merchant_commands'

RSpec.describe MerchantCommands do
  conn = Connection.new('test')
  merchants = conn.merchants

  before :all do
    MerchantCommands.init conn
  end

  after :each do
    merchants.delete_many
  end

  it 'cleans up its arguments' do
    [
      "' joe's bob        '",
      "joe's bob",
      "   joe's bob    "
    ].each { |arg| expect(MerchantCommands.clean_argument(arg)).to eq("joe's bob") }
  end

  it 'generates unique id' do
    name = 'Test Shoppe'
    desc = 'Test'
    MerchantCommands.add_merchant(1, name, desc)
    MerchantCommands.add_merchant(1, name, desc)

    merchant = merchants.all.skip(1).first
    expect(merchant[:merchant_id]).to eq('tt')
  end

  it 'adds merchants' do
    name = 'Test Shoppe'
    desc = 'Test'
    expect(MerchantCommands.add_merchant(1, name, desc)).to eq(true)

    merchant = merchants.all.first
    expect(merchant[:name]).to eq(name)
    expect(merchant[:description]).to eq(desc)
    expect(merchant[:items]).to eq([])
    expect(merchant[:merchant_id]).to eq('ts')
  end

  it 'adds items' do
    MerchantCommands.add_merchant(1, 'Test Shoppe', 'Test')

    expect(MerchantCommands.add_items(merchants.find_by_channel_id(1).first, 'Wand', 500, 'A wand', 1, 'Rare', 'Toothpaste', 1, 'A toothpaste', 100, 'Epic')).to eq(true)

    items = merchants.find_by_channel_id(1).first[:items]
    expect(items.length).to eq(2)
    expect(items[0][:name]).to eq('Wand')
    expect(items[0][:price]).to eq(500)
    expect(items[0][:quantity]).to eq(1)
    expect(items[0][:description]).to eq('A wand')
    expect(items[0][:rarity]).to eq('Rare')

    expect(items[1][:name]).to eq('Toothpaste')
    expect(items[1][:price]).to eq(1)
    expect(items[1][:quantity]).to eq(100)
    expect(items[1][:description]).to eq('A toothpaste')
    expect(items[1][:rarity]).to eq('Epic')
  end

  it 'lists merchants' do
    MerchantCommands.add_merchant(1, 'Test Shoppe', 'Test')

    expect(MerchantCommands.display_merchant_info(1)).to eq("Test Shoppe\r\nTest")
  end

  it 'lists inventory' do
    MerchantCommands.add_merchant(1, 'Test Shoppe', 'Test')
    merchant = merchants.find_by_channel_id(1).first
    MerchantCommands.add_items(merchant, 'Wand', 500, 'wand', 1, 'Rare', 'Toothpaste', 1, 'toothpaste', 100, 'Epic')
    res = MerchantCommands.list_inventory merchants.all.first
    expect(res).to eq("Test Shoppe has:\r\n'Wand' (Rare) - #1\r\n50.0 PP/500.0 GP/1000.0 EP/5000.0 SP/50000.0 CP GP - 1 left\r\nwand\r\n\r\n'Toothpaste' (Epic) - #1\r\n0.1 PP/1.0 GP/2.0 EP/10.0 SP/100.0 CP GP - 100 left\r\ntoothpaste")
  end
end